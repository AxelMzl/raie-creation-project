import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SchedulerComponent } from './features/scheduler/component/scheduler/scheduler.component';
import { SchedulerVueComponent } from './pages/calendar/scheduler-vue.component';



const routes: Routes = [
  { path: 'drop', component: SchedulerComponent },
  { path: 'reserve', component: SchedulerVueComponent },
  
  { path: '', redirectTo: '/reserve', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
