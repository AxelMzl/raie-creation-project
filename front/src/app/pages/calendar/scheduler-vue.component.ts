import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-scheduler-vue',
  templateUrl: './scheduler-vue.component.html',
  styleUrls: ['./scheduler-vue.component.scss']
})
export class SchedulerVueComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  events: Event[] = [];
      onNewEvent(event: Event) {
        this.events = this.events.concat(event);
      }

}
