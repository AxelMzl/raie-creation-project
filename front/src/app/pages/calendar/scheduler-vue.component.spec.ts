import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SchedulerVueComponent } from './scheduler-vue.component';

describe('SchedulerVueComponent', () => {
  let component: SchedulerVueComponent;
  let fixture: ComponentFixture<SchedulerVueComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SchedulerVueComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SchedulerVueComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
