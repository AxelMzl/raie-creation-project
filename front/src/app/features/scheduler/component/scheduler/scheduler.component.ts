import { Component, OnInit, Input } from '@angular/core';

export interface Event {
  Title: String;
  FirstName: String;
  Phone: BigInteger;
  Description: String;
  Mail: String;
  Start: Date;  
  End: Date;
}

@Component({
  selector: 'app-scheduler',
  templateUrl: './scheduler.component.html',
  styleUrls: ['./scheduler.component.scss']
})
export class SchedulerComponent implements OnInit {

  constructor() { }

  @Input() events: Event[];
      startTime = '9:00';
      selectedDate = new Date();
      ngOnInit() {
      }

}
