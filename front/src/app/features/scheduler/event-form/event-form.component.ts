import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';


@Component({
  selector: 'app-event-form',
  templateUrl: './event-form.component.html',
  styleUrls: ['./event-form.component.scss']
})
export class EventFormComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  eventForm: FormGroup = new FormGroup({
    title: new FormControl(''),
    firstname: new FormControl(''),
    phone: new FormControl(''),
    description: new FormControl(''),
    mail: new FormControl(''),
    start: new FormControl(),
    end: new FormControl()
  });

  @Output()
  newEvent: EventEmitter<Event> = new EventEmitter();

  handleSubmit() {
    const event = this.eventForm.value;
    this.newEvent.emit({
      ...event,
    });
    this.eventForm.reset();
  }

}
