# RAIE-CREATION-PROJECT

## Lancer le back

- cd ./back
- npm install
- npx sequelize db:migrate
- Setup une base de donnée mysql sur le port 3308 avec comme nom "coiffeur"
- Vérifier le fichier config
- nodemon .\index.js

## Test API

- JSON pour la méthode create
{
	"user_name": "test" ,
	"user_firstname": "test!",
	"user_email": "yes@yes.yes",
	"rdv_date": "18102020"
}

- POST localhost:3000/api/create - NE FONCTIONNE PAS
- GET localhost:3000/api/getListe - FONCTIONNE MAIS VIDE PAR DEFAUT

## Lancer le front

- cd ./front
- npm install
- npm start
- Go to localhost:4200