'use strict'
module.exports = (Sequelize, DataTypes) => {
    const Rdvs = Sequelize.define(
        'Rdvs',
        {
            user_name: DataTypes.STRING,
            user_firstname: DataTypes.STRING,
            user_email: DataTypes.STRING,
            rdv_date: DataTypes.STRING,
        },
        {}
    )
    Rdvs.associate = function(models) {
        // associations can be defined here
    }

    return Rdvs
}