const Rdv = require('../models/rdv').Rdvs

module.exports = (express) => {
    const liste = express.Router()
        
    liste.post('/create', async (req, res, next) => {
        try {
            let rdv = req.body
            console.log("Rdv ID : " + rdv.id);
            console.log("Rdv : " + rdv);

            
            // Create an Rdv
            rdvId = await Rdv.create(rdv)

            res.status(201).json({ rdv })
        } catch (e) {
            next(e)
        }
    })


    liste.get('/getListe',  async (req, res, next) => {
        console.log("wait a moment getList run you're request...");
        
        try {
            res.send(await Rdv.findAll())
        } catch (e) {
            res.status(500).send(e)
        }
    })

    liste.get('/:id', async (req, res, next) => {
        try {
            const rdv = await Rdv.scope(req.query['scope']).findByPk(
                req.params['id']
            )
            res.send(rdv)
        } catch (e) {
            res.status(500).send(e)
        }
    })

    liste.put('/:id', async (req, res, next) => {
        try {
            await Rdv.update(req.body, { where: { id: req.params['id'] } })
            res.sendStatus(200)
        } catch (e) {
            next(e)
        }
    })

    return liste
}